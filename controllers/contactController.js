var nodemailer = require('nodemailer');
var Contact = require('../models/contact');

exports.create_contact = function(req, res, next) {
    req.checkBody('name', 'Name must not be empty.').notEmpty();
    req.checkBody('email', 'Email must not be empty.').notEmpty();

    req.sanitize('name').escape();
    req.sanitize('name').trim();

    req.sanitize('email').escape();
    req.sanitize('email').trim();

    var contact = new Contact({
        name: req.body.name,
        email: req.body.email
    });

    var name = contact.name;
    var email = contact.email;
    console.log('CONTACT: ' +contact);

    contact.save(function(err) {
        if (err)
            res.send(err);
        sendContactEmail(name, email)
        res.redirect('/');
    });
}

exports.list_contacts = function(req, res, next) {
    Contact.find(function(err, contacts) {
        if (err)
            res.send(err);

        res.render('contacts',{ contacts: contacts });
    });
}

// Use an ESP to send our email
function sendContactEmail(name, email) {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'devaris@fga.io',
            pass: process.env.GMAIL_PASS
        }
    });

    const mailOptions = {
        from: 'devaris@fga.io', // sender address
        to: 'devaris@fga.io', // list of receivers
        subject: 'New Contact', // Subject line
        text: name + ' ' + email// plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
    });
};
    