// call the packages we need
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
require('dotenv').config()

var index = require('./routes/index');
var contacts = require('./routes/contacts');
var compression = require('compression');
var helmet = require('helmet');

// create express app object
var app = express();

// secure our application
// read for more info: https://github.com/helmetjs/helmet
app.use(helmet());

// set up mongoose connection
var mongoose = require('mongoose');
var db_url = 'mongodb://localhost:27017/contacts';
var mongoDB = process.env.MONGODB_URI || db_url;
mongoose.connect(mongoDB, {
    useMongoClient: true
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// set up view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set up logging to use dev level notification
app.use(logger('dev'));
// configure the app to use bodyParser
// allows you to get data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// set up the app to use express validation
app.use(expressValidator());
// parse Cookie header and handle cookie separation, encoding, & decryption
app.use(cookieParser());

// compress all routes
app.use(compression());

// public folder holds CSS & images
app.use(express.static(path.join(__dirname, 'public')));

// add index & contacts route to middleware
app.use('/', index);
app.use('/contacts', contacts);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error', {error: err});
});

// set our port
var port = process.env.PORT || 9000;

// start the server
app.listen(port);
console.log('Magic happens on port ' + port);