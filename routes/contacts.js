var express = require('express');
var router = express.Router();

var contact_controller = require('../controllers/contactController');

/* POST create new contact */
router.post('/', contact_controller.create_contact);

/* GET list all contacts */
router.get('/', contact_controller.list_contacts);

module.exports = router;